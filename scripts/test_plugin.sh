#!/usr/bin/env bash

mkdir -p ./bin
CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -buildmode=plugin -o bin/encryptingplugin.so test/plugin/main.go