/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	"context"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/logging"
	"github.com/djcass44/go-utils/otel"
	"github.com/djcass44/go-utils/otel/metrics"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
	"gitlab.com/autokubeops/serverless"
	"gitlab.com/av1o/cap10/pkg/api"
	"gitlab.com/av1o/cap10/pkg/filter"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"gitlab.com/av1o/cap10/pkg/passport"
	"gitlab.com/av1o/cap10/pkg/plugins"
	"gitlab.com/av1o/cap10/pkg/proxy"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"strings"
)

func main() {
	// setup flags
	port := flag.Int("port", 8080, "port to run on")
	_ = flag.Bool("debug", false, "enables debug logging")
	logLevel := flag.Int("log-level", 0, "log level (higher is more)")
	baseURL := flag.String("base-url", "", "base application url")
	enableMetrics := flag.Bool("enable-metrics", true, "enables prometheus metrics on /metrics (default: true)")

	pkiEnforcing := flag.Bool("pki-enforcing", false, "requests without a certificate will be rejected")
	pkiFilterMode := flag.String("pki-filter-mode", string(ingress.FilterNGINX), "method to extract user information from upstream")

	escapedHeader := flag.String("escaped-cert-header", "ssl-client-cert", "header to extract the full client certificate from")

	httpUserHeader := flag.String("http-user-header", "X-Auth-User", "http header to place user information")
	httpSourceHeader := flag.String("http-source-header", "X-Auth-Source", "http header to place auth issuer information")
	httpVerifyHeader := flag.String("http-verify-header", "X-Auth-Verify", "http header to place authenticity token")
	httpVerifyHashHeader := flag.String("http-verify-hash-header", "X-Auth-Hash-Verify", "http header to place authenticity hash")
	httpClaimPrefix := flag.String("http-claim-prefix", "X-Auth-", "prefix to use when extracting additional user claims")

	appendChain := flag.Bool("append-chain", false, "whether to append or replace user information (default: false)")

	oidcIssuerURL := flag.String("oidc-issuer-url", "", "the OIDC/OAuth2 issuer URL")
	oidcClientID := flag.String("oidc-client-id", "", "the OIDC/OAuth2 Client ID")
	oidcClientSecret := flag.String("oidc-client-secret", "", "the OIDC/OAuth2 Client secret")
	oidcRedirectURL := flag.String("oidc-redirect-url", "", "the OIDC/OAuth2 redirect URL")
	oidcUserRedirect := flag.String("oidc-user-redirect", "/", "path to redirect the user once the OIDC/OAuth2 callback is complete")
	oidcFetchUserInfo := flag.Bool("oidc-fetch-userinfo", true, "whether to fetch (and cache) userinfo on each request")
	oidcCookieSameSite := flag.String("oidc-cookie-same-site", "Lax", "OIDC cookie SameSite mode (default: Lax) - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite")
	oidcScopes := flag.String("oidc-scopes", "", "Comma-separated list of OIDC scopes - https://openid.net/specs/openid-connect-basic-1_0.html#Scopes")

	proxyTarget := flag.String("proxy-target", "http://localhost:8081", "the url of the server we're proxying to")
	_ = flag.String("proxy-probe-path", "/", "path to use for probing the proxy target (default: /)")
	_ = flag.Bool("proxy-skip-tls-verify", false, "skips TLS verification of secure backends (default: false)")

	passportFilePath := flag.String("passport-file-path", "", "path to a pre-generated ed25519 private key (leave empty to auto-generate)")

	pluginDirs := flag.String("plugin-dirs", "", "comma-separated list of directories to search for plugins")

	otelEnabled := flag.Bool("otel", false, "enables OpenTelemetry integration")
	otelEnv := flag.String("otel-env", "", "OpenTelemetry environment name")
	otelSampleRate := flag.Float64("otel-sample-rate", 0, "OpenTelemetry sample rate")

	flag.Parse()

	// configure logging
	zc := zap.NewProductionConfig()
	zc.Level = zap.NewAtomicLevelAt(zapcore.Level(*logLevel * -1))
	log, ctx := logging.NewZap(context.TODO(), zc)

	err := otel.Build(ctx, otel.Options{
		Enabled:     *otelEnabled,
		ServiceName: "cap10",
		Environment: *otelEnv,
		SampleRate:  *otelSampleRate,
	})
	if err != nil {
		log.Error(err, "failed to setup OpenTelemetry")
		os.Exit(1)
		return
	}
	prom := metrics.MustNewDefault(ctx)

	// setup structs
	filterOpts := &filter.Opts{
		SubjectHeader:     *httpUserHeader,
		SourceHeader:      *httpSourceHeader,
		VerifyHeader:      *httpVerifyHeader,
		VerifyHashHeader:  *httpVerifyHashHeader,
		ClaimPrefixHeader: *httpClaimPrefix,
		Append:            *appendChain,
	}

	// set up the oidc filter - it will skip itself if the issuer url is blank
	oidcFilter, err := filter.NewOIDCFilter(ctx, *baseURL, *oidcIssuerURL, &filter.OIDCCreateOpts{
		ClientID:       *oidcClientID,
		ClientSecret:   *oidcClientSecret,
		RedirectURL:    *oidcRedirectURL,
		UserRedirect:   *oidcUserRedirect,
		FetchUserInfo:  *oidcFetchUserInfo,
		CookieSameSite: *oidcCookieSameSite,
		Scopes:         strings.Split(*oidcScopes, ","),
	})
	if err != nil {
		log.Error(err, "failed to setup oidc provider")
		os.Exit(1)
		return
	}

	// se tup the key provider
	var keyProvider *passport.KeyProvider
	if *passportFilePath == "" {
		// auto generate a key
		keyProvider, err = passport.NewKeyProvider(ctx)
	} else {
		// get an existing key from a file
		keyProvider, err = passport.NewFileKeyProvider(ctx, *passportFilePath)
	}
	// bail out if there were any key-related issues
	if err != nil {
		log.Error(err, "failed to setup key provider")
		os.Exit(1)
		return
	}

	var validatingPlugins []plugins.ValidatingPlugin
	var mutatingPlugins []plugins.MutatingPlugin
	if *pluginDirs != "" {
		l := plugins.NewLoader(ctx, *pluginDirs)
		if err := l.LoadAll(); err != nil {
			log.Error(err, "failed to initialise plugins")
			os.Exit(1)
			return
		}
		validatingPlugins = l.LookupV()
		mutatingPlugins = l.LookupM()
	}

	pkiFilter := filter.NewPKIFilter(ingress.FilterMode(*pkiFilterMode), *pkiEnforcing, filter.DefaultFilters(*escapedHeader)...)
	// only 1 filter can run at a time
	// to stop user impersonation
	var f filter.ChainFilter
	if *oidcIssuerURL != "" {
		log.Info("OIDC filter is active - disabling PKI")
		f = oidcFilter
	} else {
		log.Info("OIDC filter is disabled - enabling PKI")
		f = pkiFilter
	}
	chain := filter.NewChain(filterOpts, keyProvider, f)
	handler, err := proxy.NewHandler(ctx, *proxyTarget, validatingPlugins, mutatingPlugins)
	if err != nil {
		log.Error(err, "failed to setup proxy handler")
		return
	}

	// setup router
	router := mux.NewRouter()
	router.Use(otel.Middleware(), logging.Middleware(log), metrics.Middleware())

	api.AddHealthRoute(router)
	if err = api.AddPassportRoute(keyProvider, router); err != nil {
		log.Error(err, "failed to setup passport route")
		os.Exit(1)
		return
	}
	oidcFilter.AddRoutes(ctx, router)
	if *enableMetrics {
		log.Info("exporting prometheus metrics on /metrics, set -enable-metrics=false to disable")
		router.Handle("/metrics", prom)
	}
	// generic proxy handler which runs our filters and then passes the request on
	router.PathPrefix("/").Handler(tracer.NewHandler(chain.NewHandler(handler)))

	// start http server
	serverless.NewBuilder(router).
		WithPort(*port).
		WithLogger(log).
		Run()
}
