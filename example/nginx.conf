worker_processes    1;

error_log           /var/log/nginx/error.log warn;
pid                 /tmp/nginx.pid;

events {
    worker_connections  1024;
}
http {
    include         /etc/nginx/mime.types;
    default_type    application/octet-stream;
    server_tokens   off;

    sendfile        off;

    keepalive_timeout   60;

    client_body_temp_path /tmp/client_temp;
    proxy_temp_path       /tmp/proxy_temp_path;
    fastcgi_temp_path     /tmp/fastcgi_temp;
    uwsgi_temp_path       /tmp/uwsgi_temp;
    scgi_temp_path        /tmp/scgi_temp;

    #gzip           on;
    server {
        listen      12001 http2 ssl;
        server_name localhost;

        ssl_protocols TLSv1.3;

        ssl_certificate /etc/nginx/certificates/cert.crt;
        ssl_certificate_key /etc/nginx/certificates/cert.key;
        ssl_client_certificate /etc/nginx/certificates/ca.crt;
        ssl_verify_client on;

        location ~ ^/(v3|swagger)/ {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-Forwarded-Proto https;

            proxy_set_header ssl-client-verify $ssl_client_verify;
            proxy_set_header ssl-client-subject-dn $ssl_client_s_dn;
            proxy_set_header ssl-client-issuer-dn $ssl_client_i_dn;

            proxy_pass http://localhost:12000;
        }

        location / {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-Forwarded-Proto https;
            proxy_pass http://localhost:3000;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
    }
}
