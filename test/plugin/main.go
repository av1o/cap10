/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	"github.com/go-logr/logr"
	"gitlab.com/av1o/cap10/pkg/plugins"
	"net/http"
)

type httpsValidator struct {
	plugins.ValidatingPlugin
}

func (v *httpsValidator) IsValid(r *http.Request) (bool, error) {
	log := logr.FromContextOrDiscard(r.Context())
	if r.URL.Scheme != "https" {
		log.Info("rejecting non HTTPS request")
	}
	return r.URL.Scheme == "https", nil
}

//goland:noinspection GoUnusedGlobalVariable
var ValidatingPlugin httpsValidator
