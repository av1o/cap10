# CAP10

CAP10 (Captain) is an auth proxy designed for moving authentication away from your application.
It currently supports the following authentication modes:

| Name | Status |
|------|--------|
| PKI  | Stable |
| OIDC | Beta   |

> CAP10 is not a service-mesh, nor is it an API gateway.
> CAP10 does not provide authorisation, only authentication.

CAP10 is designed for the Kubernetes age, and therefore works in tandem with your Ingress Controller.
It supports a number of Ingress frontends:
* NGINX Ingress Controller
* HAProxy
* Generic escaped certificates
* Raw TLS


## Plugins

CAP10 has a plugin system, which allows arbitrary actions to be taken when proxying a request.
Plugins are loaded using the `plugins-dir` flag or `PLUGINS_DIR` environment variable which contains a comma-separated
list of directories to search for `.so` plugin files.

*Note: plugins require building CAP10 with `CGO_ENABLED=1`*

### Mutating plugins

Mutating plugins allow you to make arbitrary changes to an `http.Request` before it is
proxied.

*Note: Cap10 makes its own changes AFTER all plugins have been invoked (e.g. setting proxy headers)*

```go
package test

import "net/http"

type encryptingMutator struct{}

func (m *encryptingMutator) Mutate(r *http.Request) error {
	if r.Host == "mydomain.internal" {
		r.URL.Scheme = "https"
	}
	return nil
}
```

### Validating plugins

Validating plugins allow you to verify that a request should be allowed through the proxy.
Validating plugins are always called AFTER mutating plugins to ensure that no validation can be broken
by a mutation.


```go
package test

import "net/http"

type httpsValidator struct{}

func (v *httpsValidator) IsValid(r *http.Request) (bool, error) {
	return r.URL.Scheme == "https", nil
}
```