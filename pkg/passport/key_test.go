/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package passport

import (
	"context"
	"github.com/djcass44/go-utils/utilities/cryptoutils"
	"github.com/go-logr/logr"
	"github.com/go-logr/logr/testr"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

var pemPublicKey = `-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEAwXHAsO+gSuBl61cxjndzjLOjFJS8JGD6IAxzADku708=
-----END PUBLIC KEY-----
`

var pemPublicKeyHash = "b4dcd118319899811775643a1daf42b37fb28f4fe980082ee5d67db4ccf95744"

func TestNewKeyProvider(t *testing.T) {
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	kp, err := NewKeyProvider(ctx)
	assert.NoError(t, err)
	assert.NotNil(t, kp.publicKey)
	assert.NotNil(t, kp.privateKey)

	// key can be retrieved
	key, err := kp.ToString()
	assert.NoError(t, err)
	assert.NotEmpty(t, key)

	assert.NotEmpty(t, kp.GetPublicKeyHash())

	// signing works without error
	res, err := kp.Sign([]byte("this is a test"))
	assert.NoError(t, err)
	assert.NotNil(t, res)
}

func BenchmarkKeyProvider_Sign(b *testing.B) {
	ctx := logr.NewContext(context.TODO(), testr.New(nil))
	kp, err := NewKeyProvider(ctx)
	if err != nil {
		b.Fatal(err)
	}
	message := []byte("Hello, world!")
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = kp.Sign(message)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func TestKeyProvider_ToString(t *testing.T) {
	key, err := cryptoutils.ParseED25519PublicKey(testr.New(t), []byte(pemPublicKey))
	assert.NoError(t, err)

	kp := &KeyProvider{publicKey: *key}

	keyStr, err := kp.ToString()
	assert.NoError(t, err)

	assert.EqualValues(t, pemPublicKey, keyStr)
}

func TestKeyProvider_GetPublicKeyHash(t *testing.T) {
	key, err := cryptoutils.ParseED25519PublicKey(testr.New(t), []byte(pemPublicKey))
	assert.NoError(t, err)

	kp := &KeyProvider{publicKey: *key}
	hash := kp.GetPublicKeyHash()

	assert.EqualValues(t, pemPublicKeyHash, hash)
}

func TestNewFileKeyProvider(t *testing.T) {
	var cases = []struct {
		name string
		path string
		v    func(t *testing.T, kp *KeyProvider, err error)
	}{
		{
			"valid key is loaded",
			"./testdata/test25519.pem",
			func(t *testing.T, kp *KeyProvider, err error) {
				assert.NoError(t, err)
				assert.NotNil(t, kp)
				assert.NotNil(t, kp.privateKey)
				assert.NotNil(t, kp.GetPublicKey())
			},
		},
		{
			"invalid keys are read but not parsed",
			"./testdata/test.pem",
			func(t *testing.T, kp *KeyProvider, err error) {
				assert.ErrorIs(t, err, cryptoutils.ErrMissingOrBadBlock)
				assert.Nil(t, kp)
			},
		},
		{
			"invalid file is not read",
			"./testdata/THISDOESNOTEXIST.pem",
			func(t *testing.T, kp *KeyProvider, err error) {
				assert.ErrorIs(t, err, os.ErrNotExist)
				assert.Nil(t, kp)
			},
		},
	}
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			kp, err := NewFileKeyProvider(ctx, tt.path)
			tt.v(t, kp, err)
		})
	}
}
