/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"crypto/x509"
	"encoding/pem"
	"github.com/djcass44/go-utils/utilities/httputils"
	"github.com/go-logr/logr"
	"net/http"
	"net/url"
)

// EscapedCertFilter retrieves user information from a full client certificate in PEM format
type EscapedCertFilter struct {
	header string // http header containing the certificate
}

func NewEscapedCertFilter(header string) *EscapedCertFilter {
	return &EscapedCertFilter{
		header: header,
	}
}

func (*EscapedCertFilter) String() string {
	return string(FilterEscapedCert)
}

func (f *EscapedCertFilter) Filter(r *http.Request) *UserInfo {
	log := logr.FromContextOrDiscard(r.Context()).WithValues("Mode", FilterEscapedCert)
	raw := r.Header.Get(f.header)
	httputils.RemoveHeaders(&r.Header, []string{f.header})
	// check that the header has any content
	if raw == "" {
		log.Info("pki header contained no content", "Header", f.header)
		return nil
	}
	// unescape the cert
	raw, err := url.QueryUnescape(raw)
	if err != nil {
		log.Error(err, "failed to escape pki header content")
		return nil
	}
	// try to parse the certificate
	block, _ := pem.Decode([]byte(raw))
	if block == nil {
		log.Error(err, "failed to decode certificate: block was nil")
		log.V(2).Info("raw certificate", "Raw", raw)
		return nil
	}
	log.V(1).Info("found certificate with type", "Type", block.Type)
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		log.Error(err, "failed to parse x509 certificate")
		return nil
	}
	log.V(1).Info("successfully parsed x509 certificate")
	// get necessary cert information and put it into the request headers
	subject := cert.Subject.String()
	issuer := cert.Issuer.String()
	log.Info("detected identity in request", "Sub", subject, "Iss", issuer)
	return &UserInfo{
		Sub: subject,
		Iss: issuer,
	}
}
