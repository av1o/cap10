/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

/**
Test certs can be generated using the following command
---
cat tls.crt | python3 -c "import urllib.parse, sys; print(urllib.parse.quote(sys.stdin.read()))" > tls_escaped.txt
*/

// interface guard
var _ DomainFilter = &EscapedCertFilter{}

//go:embed testdata/client_escaped.txt
var clientPublicKey string

//go:embed testdata/server_escaped.txt
var serverPublicKey string

var certTests = []struct {
	name string
	cert string
	pass bool
}{
	{"Client cert", clientPublicKey, true},
	{"Server cert", serverPublicKey, false},
	{"No cert", "", false},
	{"Non url-encoded text", "!@#$!@#$%^", false},
	{"Part of cert", "-----BEGIN PUBLIC KEY-----", false},
}

func TestEscapedCertFilter_Filter(t *testing.T) {
	header := "X-Some-Header"
	filter := NewEscapedCertFilter(header)
	for _, tt := range certTests {
		t.Run(tt.name, func(t *testing.T) {
			request := &http.Request{
				Header: map[string][]string{
					header: {tt.cert},
				},
			}
			ui := filter.Filter(request)
			if tt.pass {
				assert.NotNil(t, ui)
				return
			}
			assert.Nil(t, ui)
		})
	}
}
