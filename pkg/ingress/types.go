/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"fmt"
	"net/http"
)

type DomainFilter interface {
	Filter(r *http.Request) *UserInfo
	fmt.Stringer
}

// UserInfo is the base amount of information
// required to uniquely identify a user
type UserInfo struct {
	// Sub is the Subject of the user (e.g. CN or username)
	Sub string
	// Iss is the Issuer of the user (e.g. Certificate Authority or OIDC provider)
	Iss string
	// Claims are bits of arbitrary information about a user
	Claims map[string]interface{}
}

// AsUsername returns the user in a
// storable string format of Iss/Sub
func (ui *UserInfo) AsUsername() string {
	return fmt.Sprintf("%s/%s", ui.Iss, ui.Sub)
}
