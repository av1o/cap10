/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"github.com/go-logr/logr"
	"net/http"
)

// TLSFilter retrieves user information from a TLS handshake - requires the application to be running using TLS
type TLSFilter struct{}

func (*TLSFilter) String() string {
	return string(FilterTLS)
}

func (f *TLSFilter) Filter(r *http.Request) *UserInfo {
	log := logr.FromContextOrDiscard(r.Context()).WithValues("Mode", FilterTLS)
	// check that this is a TLS request
	if r.TLS == nil {
		log.V(1).Info("request TLS state is nil")
		return nil
	}
	// check that the user presented at least 1 certificate
	if len(r.TLS.PeerCertificates) == 0 {
		log.V(1).Info("client presented no certificates")
		return nil
	}
	log.V(1).Info("client presented %d certificate(s)", len(r.TLS.PeerCertificates))
	// get the subject and issuer from the 1st cert
	subject := r.TLS.PeerCertificates[0].Subject.ToRDNSequence().String()
	issuer := r.TLS.PeerCertificates[0].Issuer.ToRDNSequence().String()
	log.Info("detected identity in request", "Sub", subject, "Iss", issuer)
	return &UserInfo{
		Sub: subject,
		Iss: issuer,
	}
}
