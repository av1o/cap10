/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"github.com/djcass44/go-utils/utilities/httputils"
	"github.com/go-logr/logr"
	"net/http"
)

var (
	HAHeaderCert   = http.CanonicalHeaderKey("X-SSL-Client-Cert")
	HAHeaderVerify = http.CanonicalHeaderKey("X-SSL-Client-Verify")
	HAHeaderSub    = http.CanonicalHeaderKey("X-SSL-Client-DN")
	HAHeaderIss    = http.CanonicalHeaderKey("X-SSL-Issuer")
)

// HAProxyFilter retrieves user information from headers provided by the OpenShift/HAProxy Ingress Controller
type HAProxyFilter struct{}

func (*HAProxyFilter) String() string {
	return string(FilterHAProxy)
}

func (f *HAProxyFilter) Filter(r *http.Request) *UserInfo {
	log := logr.FromContextOrDiscard(r.Context()).WithValues("Mode", FilterHAProxy)
	if r.Header.Get(HAHeaderCert) != "1" {
		log.V(1).Info("client did not present a certificate")
		return nil
	}
	// check that the cert is valid
	if v := r.Header.Get(HAHeaderVerify); v != "0" {
		log.Info("client presented a certificate, however it failed verification", "Verification", v)
		return nil
	}
	subject := r.Header.Get(HAHeaderSub)
	issuer := r.Header.Get(HAHeaderIss)
	// remove headers
	httputils.RemoveHeaders(&r.Header, []string{HAHeaderCert, HAHeaderVerify, HAHeaderSub, HAHeaderIss})
	log.Info("detected identity in request", "Sub", subject, "Iss", issuer)
	// haproxy will extract all the information we need
	return &UserInfo{
		Sub: subject,
		Iss: issuer,
	}
}
