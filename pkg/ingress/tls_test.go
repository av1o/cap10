/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

// interface guard
var _ DomainFilter = &TLSFilter{}

var tlsTests = []struct {
	name    string
	request *http.Request
	pass    bool
}{
	{
		"Request with valid TLS certificate",
		&http.Request{
			TLS: &tls.ConnectionState{
				PeerCertificates: []*x509.Certificate{{
					Subject: pkix.Name{
						CommonName:   "Joe Bloggs",
						Organization: []string{"Internet Widgets Pty, Ltd."},
					},
				},
				},
			},
		}, true},
	{
		"Request without TLS", &http.Request{}, false,
	},
	{
		"Request with TLS but no certificate",
		&http.Request{
			TLS: &tls.ConnectionState{PeerCertificates: []*x509.Certificate{}},
		}, false,
	},
}

func TestTLSFilter_Filter(t *testing.T) {
	filter := &TLSFilter{}
	for _, tt := range tlsTests {
		t.Run(tt.name, func(t *testing.T) {
			ui := filter.Filter(tt.request)
			if tt.pass {
				assert.NotNil(t, ui)
				return
			}
			assert.Nil(t, ui)
		})
	}
}
