/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

// interface guard
var haproxyFilter DomainFilter = &HAProxyFilter{}

func TestHAProxyFilter_FilterNoCert(t *testing.T) {
	request := &http.Request{
		Header: map[string][]string{},
	}
	assert.Nil(t, haproxyFilter.Filter(request))
}

func TestHAProxyFilter_FilterInvalidCert(t *testing.T) {
	h := http.Header{}
	h.Set("X-SSL-Client-Cert", "1")
	h.Set("X-SSL-Client-DN", "CN=joe.bloggs")
	h.Set("X-SSL-Issuer", "CN=some-issuer")
	h.Set("X-SSL-Client-Verify", "1") // invalid
	request := &http.Request{
		Header: h,
	}
	assert.Nil(t, haproxyFilter.Filter(request))
}

func TestHAProxyFilter_FilterValidCert(t *testing.T) {
	h := http.Header{}
	h.Set("X-SSL-Client-Cert", "1")
	h.Set("X-SSL-Client-DN", "CN=joe.bloggs")
	h.Set("X-SSL-Issuer", "CN=some-issuer")
	h.Set("X-SSL-Client-Verify", "0")
	request := &http.Request{
		Header: h,
	}
	info := haproxyFilter.Filter(request)
	assert.NotNil(t, info)
	assert.Equal(t, "CN=joe.bloggs", info.Sub)
	assert.Equal(t, "CN=some-issuer", info.Iss)
}
