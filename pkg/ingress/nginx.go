/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package ingress

import (
	"github.com/djcass44/go-utils/utilities/httputils"
	"github.com/go-logr/logr"
	"net/http"
)

var (
	NGINXHeaderVerify = http.CanonicalHeaderKey("ssl-client-verify")
	NGINXHeaderSub    = http.CanonicalHeaderKey("ssl-client-subject-dn")
	NGINXHeaderIss    = http.CanonicalHeaderKey("ssl-client-issuer-dn")
)

// NGINXFilter retrieves user information from headers provided by the NGINX Ingress Controller
type NGINXFilter struct{}

func (*NGINXFilter) String() string {
	return string(FilterNGINX)
}

func (f *NGINXFilter) Filter(r *http.Request) *UserInfo {
	v := r.Header.Get(NGINXHeaderVerify)
	log := logr.FromContextOrDiscard(r.Context()).WithValues(
		"Verification", v,
		"UserAgent", r.UserAgent(),
		"Mode", FilterNGINX,
	)
	// check that the cert is valid
	if v != "SUCCESS" {
		if v == "" {
			log.Info("skipping filter execution as we couldn't find the expected headers")
			return nil
		}
		log.Info("client verification returned unwanted result")
		return nil
	}
	subject := r.Header.Get(NGINXHeaderSub)
	issuer := r.Header.Get(NGINXHeaderIss)
	// remove headers
	httputils.RemoveHeaders(&r.Header, []string{NGINXHeaderVerify, NGINXHeaderSub, NGINXHeaderIss})
	log.Info("detected identity in request", "Sub", subject, "Iss", issuer)
	return &UserInfo{
		Sub: subject,
		Iss: issuer,
	}
}
