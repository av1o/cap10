/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package client

import (
	"context"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestGetClaim(t *testing.T) {
	headers := http.Header{}
	headers.Set(DefaultSubjectHeader, "<joe.bloggs>")
	headers.Set(DefaultIssuerHeader, "<oidc>")
	request := &http.Request{
		Header: headers,
	}
	claim, err := GetClaim(request)

	assert.NoError(t, err)

	assert.EqualValues(t, []string{"joe.bloggs"}, claim.Subjects)
	assert.EqualValues(t, []string{"oidc"}, claim.Issuers)
}

func TestGetClaimMissingHeader(t *testing.T) {
	headers := http.Header{}
	headers.Set(DefaultSubjectHeader, "joe.bloggs")
	// headers.Set(DefaultIssuerHeader, "oidc")
	request := &http.Request{
		Header: headers,
	}
	_, err := GetClaim(request)
	assert.Error(t, err)
}

func TestGetClaimMissingHeader2(t *testing.T) {
	headers := http.Header{}
	// headers.Set(DefaultSubjectHeader, "joe.bloggs")
	headers.Set(DefaultIssuerHeader, "oidc")
	request := &http.Request{
		Header: headers,
	}
	_, err := GetClaim(request)
	assert.Error(t, err)
}

func TestGetClaimNonAppending(t *testing.T) {
	headers := http.Header{}
	headers.Set(DefaultSubjectHeader, "joe.bloggs")
	headers.Set(DefaultIssuerHeader, "oidc")
	request := &http.Request{
		Header: headers,
	}
	claim, err := GetClaim(request)

	assert.NoError(t, err)

	assert.EqualValues(t, []string{"joe.bloggs"}, claim.Subjects)
	assert.EqualValues(t, []string{"oidc"}, claim.Issuers)
}

func TestUserClaim_GetOriginalClaim(t *testing.T) {
	claim := &ChainClaim{
		Subjects: []string{"joe.bloggs"},
		Issuers:  []string{"oidc"},
		Token:    "",
	}
	oc, err := claim.GetOriginalClaim(context.TODO())
	assert.NoError(t, err)

	assert.EqualValues(t, "joe.bloggs", oc.Sub)
	assert.EqualValues(t, "oidc", oc.Iss)
}

func TestUserClaim_GetOriginalClaimFromEmpty(t *testing.T) {
	claim := &ChainClaim{
		Subjects: []string{},
		Issuers:  []string{},
		Token:    "",
	}
	_, err := claim.GetOriginalClaim(context.TODO())
	assert.Error(t, err)
}

func TestGetPieces(t *testing.T) {
	chain := "<joe.bloggs><test><>"
	assert.EqualValues(t, []string{"joe.bloggs", "test"}, getPieces(chain))
}

func TestGetPiecesWithWhitespace(t *testing.T) {
	chain := "< joe.bloggs> <test>< >"
	assert.EqualValues(t, []string{"joe.bloggs", "test"}, getPieces(chain))
}

func TestGetClaims(t *testing.T) {
	h := &http.Header{
		DefaultSubjectHeader: {"<CN=Test User>"},
		"X-Auth-Foo":         {"Bar"},
		"X-Auth-Bar":         {"Foo", "Bar"},
	}
	expected := map[string]string{
		"Foo": "Bar",
		"Bar": "Foo",
	}
	assert.Equal(t, expected, getClaims(h))
}
