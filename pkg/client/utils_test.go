/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package client

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type dummyVerifier struct {
	BaseVerifier
}

func (v *dummyVerifier) IsValid(_ context.Context, _, _, _ string) bool {
	return true
}

var dummyHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte("OK"))
})

func TestClient_WithOptionalUser(t *testing.T) {
	r1 := httptest.NewRequest(http.MethodGet, "http://example.local/test", nil)
	r2 := httptest.NewRequest(http.MethodGet, "http://example.local/test", nil)
	r2.Header = http.Header{
		DefaultSubjectHeader: {"<joe.bloggs>"},
		DefaultIssuerHeader:  {"<oidc>"},
	}

	var cases = []struct {
		name string
		in   *http.Request
		out  int
	}{
		{
			"unauthenticated request is allowed",
			r1,
			http.StatusOK,
		},
		{
			"authenticated request is allowed",
			r2,
			http.StatusOK,
		},
	}

	client := NewClient(&dummyVerifier{})
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()

			// dummy handler
			client.WithOptionalUser(dummyHandler).ServeHTTP(w, tt.in)

			t.Log(w.Code)
			assert.EqualValues(t, tt.out, w.Code)
		})
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()

			// dummy handler
			client.WithOptionalUserFunc(dummyHandler).ServeHTTP(w, tt.in)

			t.Log(w.Code)
			assert.EqualValues(t, tt.out, w.Code)
		})
	}
}

var handlerTests = []struct {
	sub  string
	iss  string
	code int
}{
	{"<joe.bloggs>", "<oidc>", http.StatusOK},
	{"joe.bloggs", "", http.StatusUnauthorized},
	{"", "<oidc>", http.StatusUnauthorized},
}

func TestClient_WithUser(t *testing.T) {
	for _, tt := range handlerTests {
		t.Run(fmt.Sprintf("%s/%s = %d", tt.iss, tt.sub, tt.code), func(t *testing.T) {
			headers := http.Header{}
			headers.Set(DefaultSubjectHeader, tt.sub)
			headers.Set(DefaultIssuerHeader, tt.iss)

			request := httptest.NewRequest(http.MethodGet, "http://example.local/test", nil)
			request.Header = headers

			client := NewClient(&dummyVerifier{})
			w := httptest.NewRecorder()

			// dummy handler
			client.WithUser(dummyHandler).ServeHTTP(w, request)

			t.Log(w.Code)
			assert.EqualValues(t, tt.code, w.Code)
		})
	}
}

func TestClient_WithUserFunc(t *testing.T) {
	for _, tt := range handlerTests {
		t.Run(fmt.Sprintf("%s/%s = %d", tt.iss, tt.sub, tt.code), func(t *testing.T) {
			headers := http.Header{}
			headers.Set(DefaultSubjectHeader, tt.sub)
			headers.Set(DefaultIssuerHeader, tt.iss)

			request := httptest.NewRequest(http.MethodGet, "http://example.local/test", nil)
			request.Header = headers

			client := NewClient(&dummyVerifier{})
			w := httptest.NewRecorder()

			// dummy handler
			client.WithUserFunc(dummyHandler.ServeHTTP)(w, request)

			t.Log(w.Code)
			assert.EqualValues(t, tt.code, w.Code)
		})
	}
}

func TestGetRequesting(t *testing.T) {
	t.Run("ensure that an empty request returns a nil user/chain", func(t *testing.T) {
		r := &http.Request{}
		user, ok := GetRequestingUser(r)
		assert.Nil(t, user)
		assert.False(t, ok)
		chain, ok := GetRequestingChain(r)
		assert.Nil(t, chain)
		assert.False(t, ok)
	})
	t.Run("ensure that an authenticated user returns a valid user/chain", func(t *testing.T) {
		r := &http.Request{
			Header: http.Header{
				DefaultSubjectHeader: []string{"<CN=Test User>"},
				DefaultIssuerHeader:  []string{"<CN=Test Issuer>"},
			},
		}
		client := NewClient(&dummyVerifier{})
		chain, user, err := client.getChain(r)
		assert.NoError(t, err)

		// store the chain and user
		ctx := context.WithValue(r.Context(), UserContextKey, user)
		ctx = context.WithValue(ctx, ChainContextKey, chain)
		r = r.WithContext(ctx)

		u2, ok := GetRequestingUser(r)
		assert.NotNil(t, u2)
		assert.True(t, ok)
		assert.Equal(t, "CN=Test Issuer/CN=Test User", u2.AsUsername())

		c2, ok := GetRequestingChain(r)
		assert.NotNil(t, c2)
		assert.True(t, ok)
		assert.Equal(t, "<CN=Test Issuer>/<CN=Test User>", c2.RawClaim)
	})
}

func TestGetContext(t *testing.T) {
	t.Run("ensure that an empty request returns a nil user/chain", func(t *testing.T) {
		ctx := context.TODO()
		user, ok := GetContextUser(ctx)
		assert.Nil(t, user)
		assert.False(t, ok)
		chain, ok := GetContextChain(ctx)
		assert.Nil(t, chain)
		assert.False(t, ok)
	})
	t.Run("ensure that an authenticated user returns a valid user/chain", func(t *testing.T) {
		chain := &ChainClaim{
			Subjects:  []string{"CN=Test User"},
			Issuers:   []string{"CN=Test Issuer"},
			Token:     "",
			TokenHash: "",
			RawClaim:  "<CN=Test Issuer>/<CN=Test User>",
			Claims:    map[string]string{},
		}
		user, err := chain.GetOriginalClaim(context.TODO())
		assert.NoError(t, err)

		// store the chain and user
		ctx := context.WithValue(context.TODO(), UserContextKey, user)
		ctx = context.WithValue(ctx, ChainContextKey, chain)

		u2, ok := GetContextUser(ctx)
		assert.NotNil(t, u2)
		assert.True(t, ok)
		assert.Equal(t, "CN=Test Issuer/CN=Test User", u2.AsUsername())

		c2, ok := GetContextChain(ctx)
		assert.NotNil(t, c2)
		assert.True(t, ok)
		assert.Equal(t, "<CN=Test Issuer>/<CN=Test User>", c2.RawClaim)
	})
}

func TestPersistUserCtx(t *testing.T) {
	user := &UserClaim{
		Sub:       "CN=Test User",
		Iss:       "CN=Test Issuer",
		Token:     "",
		TokenHash: "",
		Claims:    map[string]string{},
	}
	ctx := PersistUserCtx(context.TODO(), nil, user)
	u2, ok := GetContextUser(ctx)
	assert.NotNil(t, u2)
	assert.True(t, ok)
	assert.Equal(t, user.AsUsername(), u2.AsUsername())
}
