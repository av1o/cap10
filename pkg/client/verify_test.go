/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package client

import (
	"context"
	"fmt"
	"github.com/djcass44/go-utils/utilities/cryptoutils"
	"github.com/go-logr/logr/testr"
	"github.com/stretchr/testify/assert"
	"testing"
)

// interface guard
var _ BaseVerifier = &Verifier{}

var pemPublicKey = `-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEAwXHAsO+gSuBl61cxjndzjLOjFJS8JGD6IAxzADku708=
-----END PUBLIC KEY-----
`

var (
	msg       = "CN=dcas.dev Root CA,O=dcas.dev,L=Canberra,ST=ACT,C=AU/CN=Django Cass,O=dcas.dev,L=Canberra,ST=ACT,C=AU"
	token     = "905i2RNu77PMKBbwK/f5uyji+3SVONSPH/ICmINNSYfeIGGebMzb2NDW147/6G4q/8PDUWgdElrSadx83vA4Ag=="
	tokenHash = "b4dcd118319899811775643a1daf42b37fb28f4fe980082ee5d67db4ccf95744"
)

var verifierTests = []struct {
	token     string
	tokenHash string
	keyPem    string
	expected  bool
}{
	{token, tokenHash, pemPublicKey, true},
	{"", "", pemPublicKey, false},
	{token, tokenHash, "", false},
}

func TestVerifier_IsValid2(t *testing.T) {
	for _, tt := range verifierTests {
		t.Run(fmt.Sprintf("%v", verifierTests), func(t *testing.T) {
			verifier := Verifier{publicKeyHash: tokenHash, log: testr.New(t)}
			pk, _ := cryptoutils.ParseED25519PublicKey(testr.New(t), []byte(tt.keyPem))
			verifier.publicKey = pk

			assert.EqualValues(t, tt.expected, verifier.IsValid(context.TODO(), msg, tt.token, tt.tokenHash))
		})
	}
}
