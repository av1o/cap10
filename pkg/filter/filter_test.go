/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"context"
	"github.com/go-logr/logr"
	"github.com/go-logr/logr/testr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"gitlab.com/av1o/cap10/pkg/passport"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	HeaderSub        = "X-User-Subject"
	HeaderIss        = "X-User-Issuer"
	HeaderVerify     = "X-User-Verify"
	HeaderVerifyHash = "X-User-Verify-Hash"
	ClaimPrefix      = "X-Auth-"
)

func TestChain_NewHandler(t *testing.T) {
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	kp, err := passport.NewKeyProvider(ctx)
	assert.NoError(t, err)

	handler := http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		_, _ = w.Write([]byte("OK"))
	})
	chain := NewChain(&Opts{
		SubjectHeader:     HeaderSub,
		SourceHeader:      HeaderIss,
		VerifyHeader:      HeaderVerify,
		VerifyHashHeader:  HeaderVerifyHash,
		ClaimPrefixHeader: ClaimPrefix,
		Append:            true,
	}, kp, NewPKIFilter(ingress.FilterNGINX, true, DefaultFilters("")...))
	h := chain.NewHandler(handler)

	var cases = []struct {
		name string
		req  *http.Request
		v    func(t *testing.T, res *http.Response)
	}{
		{
			"default request is rejected",
			httptest.NewRequest(http.MethodGet, "https://example.org/foo", nil),
			func(t *testing.T, res *http.Response) {
				assert.EqualValues(t, http.StatusUnauthorized, res.StatusCode)
			},
		},
		{
			"nginx request is allowed",
			&http.Request{
				Header: http.Header{
					ingress.NGINXHeaderVerify: {"SUCCESS"},
					ingress.NGINXHeaderSub:    {"CN=Test Subject"},
					ingress.NGINXHeaderIss:    {"CN=Test Issuer"},
				},
			},
			func(t *testing.T, res *http.Response) {
				assert.EqualValues(t, http.StatusOK, res.StatusCode)
			},
		},
		{
			"haproxy request is not allowed",
			&http.Request{
				Header: http.Header{
					ingress.HAHeaderCert:   {"1"},
					ingress.HAHeaderVerify: {"0"},
					ingress.HAHeaderSub:    {"CN=Test Subject"},
					ingress.HAHeaderIss:    {"CN=Test Issuer"},
				},
			},
			func(t *testing.T, res *http.Response) {
				assert.EqualValues(t, http.StatusUnauthorized, res.StatusCode)
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			h.ServeHTTP(w, tt.req)
			res := w.Result()
			_ = res.Body.Close()
			tt.v(t, res)
		})
	}
}

func BenchmarkChain_NewHandler(b *testing.B) {
	ctx := logr.NewContext(context.TODO(), testr.New(nil))
	kp, err := passport.NewKeyProvider(ctx)
	if err != nil {
		b.Fatal(err)
	}
	handler := http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		_, _ = w.Write([]byte("OK"))
	})
	chain := NewChain(&Opts{
		SubjectHeader:     HeaderSub,
		SourceHeader:      HeaderIss,
		VerifyHeader:      HeaderVerify,
		VerifyHashHeader:  HeaderVerifyHash,
		ClaimPrefixHeader: ClaimPrefix,
		Append:            true,
	}, kp)
	h := chain.NewHandler(handler)
	req := httptest.NewRequest(http.MethodGet, "https://example.org/foo", nil)

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		w := httptest.NewRecorder()
		h.ServeHTTP(w, req)
		result := w.Result()
		_ = result.Body.Close()
		b.Log(result.Status)
	}
}

func TestAppendHeader(t *testing.T) {
	headers := http.Header{}
	headers.Set(HeaderSub, "<joe.bloggs>")
	headers.Set(HeaderIss, "<oidc>")

	appendHeader(&headers, HeaderSub, "john.appleseed")
	appendHeader(&headers, HeaderIss, "oidc")

	assert.EqualValues(t, "<joe.bloggs><john.appleseed>", headers.Get(HeaderSub))
	assert.EqualValues(t, "<oidc><oidc>", headers.Get(HeaderIss))
}

func TestAppendToEmptyHeader(t *testing.T) {
	headers := http.Header{}

	appendHeader(&headers, HeaderSub, "john.appleseed")
	appendHeader(&headers, HeaderIss, "oidc")

	assert.EqualValues(t, "<john.appleseed>", headers.Get(HeaderSub))
	assert.EqualValues(t, "<oidc>", headers.Get(HeaderIss))
}

func TestChain_Reject(t *testing.T) {
	chain := Chain{
		key: nil,
		opts: &Opts{
			SubjectHeader:     HeaderSub,
			SourceHeader:      HeaderIss,
			VerifyHeader:      HeaderVerify,
			VerifyHashHeader:  HeaderVerifyHash,
			ClaimPrefixHeader: ClaimPrefix,
			Append:            false,
		},
		filters: nil,
	}
	request := &http.Request{
		Header: http.Header{},
	}
	request.Header.Set(HeaderSub, "<joe.bloggs>")
	request.Header.Set(HeaderIss, "<oidc>")
	request.Header.Set(HeaderVerify, "hunter2")
	request.Header.Set("X-Auth-Roles", "super_user")

	chain.reject(request)

	// ensures that the headers have been wiped
	assert.Empty(t, request.Header.Get(HeaderSub))
	assert.Empty(t, request.Header.Get(HeaderIss))
	assert.Empty(t, request.Header.Get(HeaderVerify))
	assert.Empty(t, request.Header.Get(HeaderVerifyHash))
	assert.Empty(t, request.Header.Get("X-Auth-Roles"))
}

func TestChain_setOrAppend(t *testing.T) {
	var cases = []struct {
		name string
		r    *http.Request
		user *ingress.UserInfo
		v    func(t *testing.T, r *http.Request)
	}{
		{
			"nil claims are ignored",
			&http.Request{
				Header: map[string][]string{},
			},
			&ingress.UserInfo{
				Sub:    "CN=Joe Bloggs",
				Iss:    "CN=Test Issuer",
				Claims: nil,
			},
			func(t *testing.T, r *http.Request) {
				assert.EqualValues(t, "CN=Joe Bloggs", r.Header.Get(HeaderSub))
				assert.EqualValues(t, "CN=Test Issuer", r.Header.Get(HeaderIss))
			},
		},
		{
			"claims are set in headers",
			&http.Request{
				Header: map[string][]string{},
			},
			&ingress.UserInfo{
				Sub: "12345678",
				Iss: "https://gitlab.com",
				Claims: map[string]interface{}{
					"given_name":  "Joe",
					"family_name": "Bloggs",
					"name":        "Joe Bloggs",
					"something":   1,
				},
			},
			func(t *testing.T, r *http.Request) {
				assert.EqualValues(t, "12345678", r.Header.Get(HeaderSub))
				assert.EqualValues(t, "https://gitlab.com", r.Header.Get(HeaderIss))
				assert.EqualValues(t, "Bloggs", r.Header.Get("X-Auth-Family-Name"))
				assert.EqualValues(t, "1", r.Header.Get("X-Auth-Something"))
			},
		},
	}

	ctx := logr.NewContext(context.TODO(), testr.New(t))
	kp, _ := passport.NewKeyProvider(ctx)
	chain := Chain{
		key: kp,
		opts: &Opts{
			SubjectHeader:     HeaderSub,
			SourceHeader:      HeaderIss,
			VerifyHeader:      HeaderVerify,
			VerifyHashHeader:  HeaderVerifyHash,
			ClaimPrefixHeader: ClaimPrefix,
			Append:            false,
		},
		filters: nil,
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			// clone the request so we can modify it safely
			req := tt.r.WithContext(ctx)
			err := chain.setOrAppend(req, tt.user)
			assert.NoError(t, err)
			tt.v(t, req)
		})
	}
}
