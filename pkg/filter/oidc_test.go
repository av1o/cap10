/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestOIDCFilter_saveToken(t *testing.T) {
	f := &OIDCFilter{
		cookieURL: "example.org",
	}
	w := httptest.NewRecorder()
	err := f.saveToken(w, OIDCTokenName, "thisistotallyatoken")
	assert.NoError(t, err)

	res := w.Result()
	assert.Len(t, res.Cookies(), 1)
	c := res.Cookies()[0]
	assert.EqualValues(t, OIDCTokenName, c.Name)
	assert.EqualValues(t, "thisistotallyatoken", c.Value)
	// check security settings
	assert.True(t, c.HttpOnly)
	assert.True(t, c.Secure)
}

func TestOIDCFilter_getToken(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "https://example.org", nil)
	r.AddCookie(&http.Cookie{
		Name:     OIDCTokenName,
		Value:    "token",
		HttpOnly: true, // prevent js access
		Domain:   "https://example.org",
		Secure:   true, // require https except on localhost
		Path:     "/",  // cover the entire domain
		SameSite: http.SameSiteLaxMode,
		Expires:  time.Now().AddDate(999, 0, 0),
	})
	f := &OIDCFilter{}
	token, err := f.getToken(r, OIDCTokenName)
	assert.NoError(t, err)
	assert.EqualValues(t, "token", token)

	_, err = f.getToken(r, OIDCIDName)
	assert.Error(t, err)
}

func TestSafeHeader(t *testing.T) {
	var cases = []struct {
		in  string
		out string
	}{
		{
			"X-Auth-https://gitlab.org/claims/groups/owner",
			"X-Auth-https---gitlab.org-claims-groups-owner",
		},
	}
	for _, tt := range cases {
		t.Run(tt.in, func(t *testing.T) {
			out := SafeHeader(tt.in)
			assert.EqualValues(t, tt.out, out)
		})
	}
}
