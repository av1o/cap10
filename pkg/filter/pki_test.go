/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewPKIFilter(t *testing.T) {
	// create PKIFilter with default filters
	assert.NotNil(t, NewPKIFilter(ingress.FilterTLS, true, DefaultFilters("")...).filter)

	// create PKIFilter with extra filter
	assert.NotNil(t, NewPKIFilter("foobar", true, DefaultFilters("")...))
}

func TestPKIFilter_DoFilter(t *testing.T) {
	var cases = []struct {
		name    string
		headers http.Header
		pass    bool
	}{
		{
			"No headers", http.Header{}, false,
		},
	}
	filter := NewPKIFilter(ingress.FilterNGINX, true, DefaultFilters("")...)

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "https://example.org/foo", nil)
			req.Header = tt.headers
			w := httptest.NewRecorder()

			// execute the filter
			ui, err := filter.DoFilter(w, req)
			result := w.Result()
			_ = result.Body.Close()
			// check the result
			if tt.pass {
				assert.NoError(t, err)
				assert.NotNil(t, ui)
				return
			}
			assert.EqualValues(t, http.StatusUnauthorized, result.StatusCode)
			assert.Error(t, err)
			assert.Nil(t, ui)
		})
	}
}
