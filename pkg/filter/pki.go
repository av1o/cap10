/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"errors"
	"github.com/go-logr/logr"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"net/http"
)

type PKIFilter struct {
	mode      ingress.FilterMode
	enforcing bool
	filter    ingress.DomainFilter
}

func DefaultFilters(escapedHeader string) []ingress.DomainFilter {
	return []ingress.DomainFilter{
		&ingress.NGINXFilter{},
		&ingress.HAProxyFilter{},
		&ingress.TLSFilter{},
		ingress.NewEscapedCertFilter(escapedHeader),
	}
}

func NewPKIFilter(mode ingress.FilterMode, enforcing bool, supportedFilters ...ingress.DomainFilter) *PKIFilter {
	f := new(PKIFilter)
	f.mode = mode
	f.enforcing = enforcing
	f.filter = f.getFilter(supportedFilters, mode)

	return f
}

func (f *PKIFilter) DoFilter(w http.ResponseWriter, r *http.Request) (*ingress.UserInfo, error) {
	log := logr.FromContextOrDiscard(r.Context())
	if user := f.filter.Filter(r); user != nil {
		log.V(1).Info("filter located user information, no further filters will run", "Filter", f.mode)
		return user, nil
	}
	log.Info("no ingress filters matched request", "UserAgent", r.UserAgent())
	return nil, f.reject(w)
}

// reject throws a 401 error if in enforcing mode
func (f *PKIFilter) reject(w http.ResponseWriter) error {
	if f.enforcing {
		http.Error(w, "401 unauthorised", http.StatusUnauthorized)
		return errors.New("unauthorised access blocked - invalid cert or no cert provided")
	}
	return nil
}

func (*PKIFilter) getFilter(filters []ingress.DomainFilter, mode ingress.FilterMode) ingress.DomainFilter {
	for _, f := range filters {
		if f.String() == string(mode) {
			return f
		}
	}
	return &ingress.NGINXFilter{}
}
