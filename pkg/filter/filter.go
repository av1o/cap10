/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"encoding/base64"
	"fmt"
	"github.com/go-logr/logr"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"gitlab.com/av1o/cap10/pkg/passport"
	"net/http"
	"reflect"
	"strings"
)

type Opts struct {
	SubjectHeader    string // http header to put the user subject
	SourceHeader     string // http header to indicate what the subject means (e.g. to distinguish oauth2 vs pki)
	VerifyHeader     string // http header to put authenticity signature
	VerifyHashHeader string // http header to put authenticity hash
	// ClaimPrefixHeader is the prefix for http headers
	// containing user claims
	ClaimPrefixHeader string
	Append            bool
}

type ChainFilter interface {
	DoFilter(http.ResponseWriter, *http.Request) (*ingress.UserInfo, error)
}

type Chain struct {
	key     *passport.KeyProvider
	keyHash string
	opts    *Opts
	filters []ChainFilter
}

func NewChain(opts *Opts, key *passport.KeyProvider, filters ...ChainFilter) *Chain {
	c := new(Chain)
	c.key = key
	c.keyHash = key.GetPublicKeyHash()
	c.opts = opts
	c.filters = filters

	return c
}

func (c *Chain) NewHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logr.FromContextOrDiscard(r.Context())
		// call all filters
		log.V(1).Info("handling filters before actioning request", "Filters", len(c.filters))
		var user *ingress.UserInfo
		for _, f := range c.filters {
			u, err := f.DoFilter(w, r)
			if err != nil {
				log.Error(err, "encountered error in filter", "Filter", reflect.TypeOf(f).String())
				return
			}
			user = u
			if user != nil {
				break
			}
		}
		if user != nil {
			if err := c.setOrAppend(r, user); err != nil {
				c.reject(r)
			}
		} else {
			c.reject(r)
		}
		// call the next function
		log.V(1).Info("filter pre-processing complete, continuing with request")
		h.ServeHTTP(w, r)
	})
}

func (c *Chain) setOrAppend(r *http.Request, user *ingress.UserInfo) error {
	log := logr.FromContextOrDiscard(r.Context())
	if !c.opts.Append {
		log.V(1).Info("overwriting user claim headers")
		log.V(2).Info("overwriting header", "Before", r.Header.Get(c.opts.SubjectHeader), "After", user.Sub)
		log.V(2).Info("overwriting header", "Before", r.Header.Get(c.opts.SourceHeader), "After", user.Iss)
		// replace the existing headers
		r.Header.Set(c.opts.SubjectHeader, user.Sub)
		r.Header.Set(c.opts.SourceHeader, user.Iss)
	} else {
		log.V(1).Info("appending user claim headers")
		log.V(2).Info("appending header", "Before", r.Header.Get(c.opts.SubjectHeader), "Append", user.Sub)
		log.V(2).Info("appending header", "Before", r.Header.Get(c.opts.SourceHeader), "Append", user.Iss)
		// append to existing headers
		appendHeader(&r.Header, c.opts.SubjectHeader, user.Sub)
		appendHeader(&r.Header, c.opts.SourceHeader, user.Iss)
	}
	// sign the iss/sub value using our key
	authString := fmt.Sprintf("%s/%s", r.Header.Get(c.opts.SourceHeader), r.Header.Get(c.opts.SubjectHeader))
	log.V(1).Info("signing token with key", "KeyHash", c.keyHash)
	verifyToken, err := c.key.Sign([]byte(authString))
	if err != nil {
		log.Error(err, "failed to generate authenticity token")
		return err
	}
	// convert the token to base64 so we can put it in a header
	vt := base64.StdEncoding.EncodeToString(verifyToken)
	log.V(1).Info("generated authenticity token for request", "Token", vt)
	r.Header.Set(c.opts.VerifyHeader, vt)
	r.Header.Set(c.opts.VerifyHashHeader, c.keyHash)

	// write the claims (if any were found)
	if user.Claims != nil {
		for k, v := range user.Claims {
			claim := fmt.Sprintf("%s%s", c.opts.ClaimPrefixHeader, strings.ReplaceAll(k, "_", "-"))
			log.V(2).Info("writing claim", "Key", k, "Header", claim, "Value", v)
			r.Header.Set(claim, fmt.Sprintf("%v", v))
		}
	}

	return nil
}

func appendHeader(headers *http.Header, key, value string) {
	headers.Set(key, fmt.Sprintf("%s<%s>", headers.Get(key), value))
}

func (c *Chain) reject(r *http.Request) {
	log := logr.FromContextOrDiscard(r.Context())
	// make sure we clean the users so users can't impersonate whoever they want
	log.V(1).Info("failed to locate user information, purging restricted headers")
	log.V(2).Info("restricted header contents", "Subject", r.Header.Get(c.opts.SubjectHeader), "Source", r.Header.Get(c.opts.SourceHeader))
	r.Header.Del(c.opts.SubjectHeader)
	r.Header.Del(c.opts.SourceHeader)
	r.Header.Del(c.opts.VerifyHeader)
	r.Header.Del(c.opts.VerifyHashHeader)
	// remove claim headers
	for k := range r.Header {
		if strings.HasPrefix(k, c.opts.ClaimPrefixHeader) {
			r.Header.Del(k)
		}
	}
}
