/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package filter

import (
	"context"
	"fmt"
	"github.com/Code-Hex/go-generics-cache"
	"github.com/Code-Hex/go-generics-cache/policy/lfu"
	"github.com/coreos/go-oidc"
	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	"gitlab.com/av1o/cap10/pkg/ingress"
	"golang.org/x/oauth2"
	"net/http"
	"regexp"
	"strings"
	"time"
)

type Claims map[string]interface{}

var HeaderKeyRegex = regexp.MustCompile(`[^\w!#$%&'*+\-.^\x60|~]`)

type OIDCFilter struct {
	enabled        bool
	config         oauth2.Config
	provider       *oidc.Provider
	verifier       *oidc.IDTokenVerifier
	cookieURL      string
	redirectPath   string
	fetchUserInfo  bool
	cookieSameSite http.SameSite

	validTokenCache *cache.Cache[string, *oidc.IDToken]
	userInfoCache   *cache.Cache[string, Claims]
}

type OIDCCreateOpts struct {
	ClientID       string
	ClientSecret   string
	RedirectURL    string
	UserRedirect   string
	FetchUserInfo  bool
	CookieSameSite string
	Scopes         []string
}

const (
	OIDCAccessTokenName = "oidc-access-token"
	OIDCTokenName       = "oidc-token"
	OIDCIDName          = "oidc-id_token"
)

func NewOIDCFilter(ctx context.Context, baseURL string, issuerURL string, opt *OIDCCreateOpts) (*OIDCFilter, error) {
	log := logr.FromContextOrDiscard(ctx)
	f := new(OIDCFilter)
	f.validTokenCache = cache.New(cache.AsLFU[string, *oidc.IDToken](lfu.WithCapacity(1000)))
	f.userInfoCache = cache.New(cache.AsLFU[string, Claims](lfu.WithCapacity(1000)))
	if issuerURL == "" {
		log.Info("skipping oidc setup as issuerURL is blank")
		f.enabled = false
		return f, nil
	}
	f.enabled = true
	provider, err := oidc.NewProvider(ctx, issuerURL)
	if err != nil {
		log.Error(err, "failed to setup OIDC provider")
		return nil, err
	}
	f.cookieURL = strings.TrimPrefix(baseURL, "https://")
	// create the verifier
	f.verifier = provider.Verifier(&oidc.Config{ClientID: opt.ClientID})
	f.provider = provider
	f.fetchUserInfo = opt.FetchUserInfo
	f.config = oauth2.Config{
		ClientID:     opt.ClientID,
		ClientSecret: opt.ClientSecret,
		RedirectURL:  opt.RedirectURL,
		// Discovery returns the OAuth2 endpoints
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OIDC flows
		Scopes: append(opt.Scopes, oidc.ScopeOpenID),
	}
	log.Info("using OIDC provider", "Issuer", issuerURL, "Scopes", f.config.Scopes)
	f.redirectPath = opt.UserRedirect

	switch opt.CookieSameSite {
	default:
		log.Info("unknown or unexpected SameSite mode - defaulting to Lax", "Mode", opt.CookieSameSite)
		fallthrough
	case "Lax":
		f.cookieSameSite = http.SameSiteLaxMode
	case "Strict":
		f.cookieSameSite = http.SameSiteStrictMode
	case "None":
		f.cookieSameSite = http.SameSiteNoneMode
	}
	log.Info("setting cookie SameSite", "Mode", f.cookieSameSite)

	return f, nil
}

func (f *OIDCFilter) AddRoutes(ctx context.Context, r *mux.Router) {
	log := logr.FromContextOrDiscard(ctx)
	if !f.enabled {
		log.V(1).Info("skipping OIDC routes")
		return
	}
	log.Info("initialising oidc http routes - these will mask the /auth prefix on the proxy target")
	r.HandleFunc("/auth/redirect", f.handleRedirect)
	r.HandleFunc("/auth/callback", f.handleCallback)
	r.HandleFunc("/auth/ping", f.handleProbe)
}

func (f *OIDCFilter) DoFilter(w http.ResponseWriter, r *http.Request) (*ingress.UserInfo, error) {
	log := logr.FromContextOrDiscard(r.Context()).WithValues("Path", r.URL.Path)
	if !f.enabled {
		return nil, nil
	}
	log.Info("checking for OIDC cookie", "UserAgent", r.UserAgent())
	accessToken, err := f.getToken(r, OIDCAccessTokenName)
	if err != nil {
		log.V(2).Error(err, "failed to extract access_token from cookie")
	}
	refreshToken, err := f.getToken(r, OIDCTokenName)
	if err != nil {
		log.V(2).Error(err, "failed to extract refresh_token from cookie")
	}
	// get the raw token from the cookie
	rawIDToken, err := f.getToken(r, OIDCIDName)
	if err != nil {
		log.V(2).Error(err, "failed to extract id_token from cookie")
		return nil, nil
	}
	idToken, err := f.isTokenValid(r.Context(), rawIDToken)
	if err != nil {
		// try to exchange the token for a new one (refresh flow)
		ts := f.config.TokenSource(r.Context(), &oauth2.Token{RefreshToken: refreshToken})
		newToken, err := ts.Token()
		if err != nil {
			log.Error(err, "failed to exchange token")
			return nil, nil
		}
		log.Info("successfully retrieved new oidc token")
		// save the new token
		if err = f.saveToken(w, OIDCTokenName, newToken.RefreshToken); err != nil {
			log.Error(err, "failed to save updated oauth2 refresh token")
		}
		// save the new token
		accessToken = newToken.AccessToken
		if err = f.saveToken(w, OIDCAccessTokenName, newToken.AccessToken); err != nil {
			log.Error(err, "failed to save update oauth2 access token")
		}
		// get the id token from our new oauth2 token
		rid, ok := newToken.Extra("id_token").(string)
		if !ok {
			log.Info("failed to get id_token from oauth2 token")
			return nil, nil
		}
		rawIDToken = rid
		// verify the new id token
		idToken, err = f.isTokenValid(r.Context(), rid)
		if err != nil {
			log.Error(err, "failed to validate refreshed token")
			return nil, nil
		}
	}
	// assume from here-on that the token is valid
	log.Info("oidc token has been successfully validated")
	if err = f.saveToken(w, OIDCIDName, rawIDToken); err != nil {
		log.Error(err, "failed to save id_token")
	}
	// parse the claims if we can
	claims := Claims{}
	if err := idToken.Claims(&claims); err != nil {
		log.Error(err, "failed to marshal claims from oidc token")
	}
	// fetch the userInfo if we've cached it
	if f.fetchUserInfo {
		userInfo, ok := f.getUserInfo(r.Context(), &oauth2.Token{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
			Expiry:       idToken.Expiry,
		}, rawIDToken)
		if ok {
			log.Info("located userinfo")
			for k, v := range userInfo {
				claims[SafeHeader(k)] = v
			}
		}
	}
	log.Info("successfully retrieved OIDC user", "Sub", idToken.Subject, "Iss", idToken.Issuer, "Claims", claims)
	// return the required info
	return &ingress.UserInfo{
		Sub:    idToken.Subject,
		Iss:    idToken.Issuer,
		Claims: claims,
	}, nil
}

// isTokenValid checks whether a given oidc id_token is valid
func (f *OIDCFilter) isTokenValid(ctx context.Context, token string) (*oidc.IDToken, error) {
	log := logr.FromContextOrDiscard(ctx)
	// check the cache
	idToken, ok := f.validTokenCache.Get(token)
	if ok {
		log.Info("located cached ID token")
		return idToken, nil
	}
	idToken, err := f.verifier.Verify(ctx, token)
	if err != nil {
		log.Error(err, "failed to verify OIDC token")
		return nil, err
	}
	// update the cache
	f.validTokenCache.Set(token, idToken, cache.WithExpiration(time.Hour))
	return idToken, nil
}

func (f *OIDCFilter) handleRedirect(w http.ResponseWriter, r *http.Request) {
	log := logr.FromContextOrDiscard(r.Context())
	uri := f.config.AuthCodeURL("state")
	log.V(1).Info("redirecting request", "Path", r.URL.Path, "Url", uri)
	http.Redirect(w, r, uri, http.StatusFound)
}

func (f *OIDCFilter) handleProbe(w http.ResponseWriter, r *http.Request) {
	log := logr.FromContextOrDiscard(r.Context())
	log.V(1).Info("answering OIDC probe", "RemoteAddr", r.RemoteAddr, "UserAgent", r.UserAgent())
	_, _ = w.Write([]byte("OK"))
}

func (f *OIDCFilter) handleCallback(w http.ResponseWriter, r *http.Request) {
	log := logr.FromContextOrDiscard(r.Context())
	oauth2Token, err := f.config.Exchange(r.Context(), r.URL.Query().Get("code"))
	if err != nil {
		log.Error(err, "failed to parse oauth2 token")
		http.Error(w, fmt.Sprintf("failed to parse oauth2 token: %s", err.Error()), http.StatusBadRequest)
		return
	}
	// extract the id token from the oauth2 token
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		msg := "id_token is missing"
		log.Info(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}
	// parse and verify the ID token payload
	idToken, err := f.verifier.Verify(r.Context(), rawIDToken)
	if err != nil {
		log.Error(err, "failed to verify OIDC token")
		http.Error(w, fmt.Sprintf("failed to verify oidc token: %s", err.Error()), http.StatusBadRequest)
		return
	}
	log.Info("identified OIDC user", "Sub", idToken.Subject, "Iss", idToken.Issuer)
	log.V(1).Info("successfully verified id token", "Token", idToken)
	_, _ = f.getUserInfo(r.Context(), oauth2Token, rawIDToken)
	// save the oauth2 token
	if err = f.saveToken(w, OIDCTokenName, oauth2Token.RefreshToken); err != nil {
		log.Error(err, "failed to save refresh_token")
	}
	// save the id token
	if err = f.saveToken(w, OIDCIDName, rawIDToken); err != nil {
		log.Error(err, "failed to save id_token")
	}
	if err = f.saveToken(w, OIDCAccessTokenName, oauth2Token.AccessToken); err != nil {
		log.Error(err, "failed to save access_token")
	}
	uri := fmt.Sprintf("https://%s%s", f.cookieURL, f.redirectPath)
	log.V(2).Info("redirecting user after successful callback", "Url", uri)
	http.Redirect(w, r, uri, http.StatusFound)
}

// getUserInfo retrieves OIDC UserInfo from the OIDC provider.
// If it has previously cached, it will return cached data.
func (f *OIDCFilter) getUserInfo(ctx context.Context, token *oauth2.Token, rawToken string) (Claims, bool) {
	log := logr.FromContextOrDiscard(ctx)
	log.V(1).Info("fetching userinfo")
	claims := Claims{}
	// check the cache
	info, ok := f.userInfoCache.Get(rawToken)
	log.V(1).Info("retrieved userinfo from cache", "Cached", ok)
	if ok {
		log.Info("located cached userinfo")
		for k, v := range info {
			claims[SafeHeader(k)] = v
		}
		return claims, true
	}
	// otherwise fetch the data manually
	userInfo, err := f.provider.UserInfo(ctx, oauth2.StaticTokenSource(token))
	if err != nil {
		log.Error(err, "failed to get userinfo")
		return nil, false
	}
	if err := userInfo.Claims(&info); err != nil {
		log.Error(err, "failed to unmarshal claims")
		return nil, false
	}
	for k, v := range info {
		claims[SafeHeader(k)] = v
	}
	log.Info("successfully retrieved user claims", "Claims", claims)
	// cache the claims
	f.userInfoCache.Set(rawToken, claims)
	return claims, true
}

// getToken retrieves a named cookie and returns its value
func (f *OIDCFilter) getToken(r *http.Request, name string) (string, error) {
	cookie, err := r.Cookie(name)
	if err != nil {
		return "", err
	}
	return cookie.Value, nil
}

// saveToken persists an oidc token to an HTTP cookie
func (f *OIDCFilter) saveToken(w http.ResponseWriter, name string, token string) error {
	// set the auth cookie
	cookie := &http.Cookie{
		Name:     name,
		Value:    token,
		HttpOnly: true, // prevent js access
		Domain:   f.cookieURL,
		Secure:   true, // require https except on localhost
		Path:     "/",  // cover the entire domain
		SameSite: f.cookieSameSite,
		Expires:  time.Now().AddDate(999, 0, 0),
	}
	http.SetCookie(w, cookie)
	return nil
}

func SafeHeader(s string) string {
	return HeaderKeyRegex.ReplaceAllString(s, "-")
}
