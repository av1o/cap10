/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package verify

import (
	"context"
	"gitlab.com/av1o/cap10/pkg/client"
)

// NoOpVerifier skips the verification mechanism
// and assumes all requests are valid
type NoOpVerifier struct {
	client.BaseVerifier
}

// NewNoOpVerifier creates a new instance of
// NoOpVerifier.
func NewNoOpVerifier() *NoOpVerifier {
	v := new(NoOpVerifier)

	return v
}

func (*NoOpVerifier) IsValid(_ context.Context, _, _, _ string) bool {
	return true
}
