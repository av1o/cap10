/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package verify

import (
	"context"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/go-logr/logr/testr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/client"
	"io/ioutil"
	"testing"
	"time"
)

// interface guard
var _ client.BaseVerifier = &FileVerifier{}

var pemPublicKey = `-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEAwXHAsO+gSuBl61cxjndzjLOjFJS8JGD6IAxzADku708=
-----END PUBLIC KEY-----
`

var (
	msg       = "CN=dcas.dev Root CA,O=dcas.dev,L=Canberra,ST=ACT,C=AU/CN=Django Cass,O=dcas.dev,L=Canberra,ST=ACT,C=AU"
	token     = "905i2RNu77PMKBbwK/f5uyji+3SVONSPH/ICmINNSYfeIGGebMzb2NDW147/6G4q/8PDUWgdElrSadx83vA4Ag==" //nolint:gosec
	tokenHash = "b4dcd118319899811775643a1daf42b37fb28f4fe980082ee5d67db4ccf95744"
)

var verifierTests = []struct {
	token     string
	tokenHash string
	expected  bool
}{
	{token, tokenHash, true},
	{"", "", false},
	{token, "", true},
	{"Nbn(+(t8ԏMI aln*QhZi|8", "", false},
}

func TestNewFileVerifier(t *testing.T) {
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	// create a verifier that points to a read file
	path := fmt.Sprintf("%s/tls.crt", t.TempDir())
	_ = ioutil.WriteFile(path, []byte(pemPublicKey), 0600)
	_, err := NewFileVerifier(ctx, path)

	assert.NoError(t, err)

	// create a verifier that points to nothing
	path = fmt.Sprintf("%s/%s", t.TempDir(), time.Now().String())
	_, err = NewFileVerifier(ctx, path)

	assert.Error(t, err)

	// create a verifier that points to a real but invalid file
	path = fmt.Sprintf("%s/tls.crt", t.TempDir())
	_ = ioutil.WriteFile(path, []byte("test"), 0600)
	_, err = NewFileVerifier(ctx, path)

	assert.Error(t, err)
}

func TestFileVerifier_IsValid(t *testing.T) {
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	path := fmt.Sprintf("%s/tls.crt", t.TempDir())
	// setup
	_ = ioutil.WriteFile(path, []byte(pemPublicKey), 0600)

	// actual tests
	for _, tt := range verifierTests {
		t.Run(fmt.Sprintf("%v", verifierTests), func(t *testing.T) {
			v, err := NewFileVerifier(ctx, path)
			assert.NoError(t, err)

			assert.EqualValues(t, tt.expected, v.IsValid(context.TODO(), msg, tt.token, tt.tokenHash))
		})
	}
}
