/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package verify

import (
	"context"
	"crypto/ed25519"
	"encoding/base64"
	"github.com/djcass44/go-utils/utilities/cryptoutils"
	"github.com/go-logr/logr"
	"gitlab.com/av1o/cap10/pkg/client"
	"os"
)

// FileVerifier uses a local file (e.g. mounted k8s secret)
// as the CAP10 public key
type FileVerifier struct {
	client.BaseVerifier
	publicKey *ed25519.PublicKey
}

// NewFileVerifier creates a new FileVerifier and
// attempts to read and parse the file at the given
// path
func NewFileVerifier(ctx context.Context, path string) (*FileVerifier, error) {
	log := logr.FromContextOrDiscard(ctx).WithValues("Path", path)
	data, err := os.ReadFile(path)
	if err != nil {
		log.Error(err, "failed to read file")
		return nil, err
	}
	key, err := cryptoutils.ParseED25519PublicKey(log, data)
	if err != nil {
		log.Error(err, "failed to parse ed25519 public key from file")
		return nil, err
	}
	v := new(FileVerifier)
	v.publicKey = key

	return v, nil
}

func (v *FileVerifier) IsValid(ctx context.Context, msg, sig, _ string) bool {
	log := logr.FromContextOrDiscard(ctx)
	bytes, err := base64.StdEncoding.DecodeString(sig)
	if err != nil {
		log.Error(err, "failed to decode base64 authenticity token '%s'", sig)
		return false
	}
	return ed25519.Verify(*v.publicKey, []byte(msg), bytes)
}
