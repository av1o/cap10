/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package api_test

import (
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/api"
	"net/http"
	"testing"
)

func TestAddHealthRoute(t *testing.T) {
	r := mux.NewRouter()
	api.AddHealthRoute(r)

	assert.HTTPSuccess(t, r.ServeHTTP, http.MethodGet, "/healthz", nil)
	assert.HTTPBodyContains(t, r.ServeHTTP, http.MethodGet, "/healthz", nil, "OK")
	assert.HTTPStatusCode(t, r.ServeHTTP, http.MethodGet, "/", nil, http.StatusNotFound)
}
