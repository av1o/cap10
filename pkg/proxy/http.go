/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package proxy

import (
	"context"
	"crypto/tls"
	"errors"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/go-logr/logr"
	"gitlab.com/av1o/cap10/pkg/plugins"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric/global"
	"go.opentelemetry.io/otel/metric/instrument"
	"go.opentelemetry.io/otel/metric/unit"
	"net/http"
	"net/http/httputil"
	"net/url"
)

var (
	meter                      = global.MeterProvider().Meter("cap10")
	metricProxyBytesReceive, _ = meter.SyncInt64().Counter(
		"cap.proxy.rx",
		instrument.WithUnit(unit.Bytes),
		instrument.WithDescription("Total number of bytes received by the proxy"),
	)
	metricProxyBytesTransmit, _ = meter.SyncInt64().Counter(
		"cap.proxy.tx",
		instrument.WithUnit(unit.Bytes),
		instrument.WithDescription("Total number of bytes transmitted by the proxy"),
	)
	metricProxyResponse, _ = meter.SyncInt64().Counter(
		"cap.proxy.response",
		instrument.WithUnit(unit.Bytes),
	)
)

var (
	xForwardedHost  = http.CanonicalHeaderKey("X-Forwarded-Host")
	xForwardedProto = http.CanonicalHeaderKey("X-Forwarded-Proto")
)

type Handler struct {
	target     *url.URL
	client     *http.Client
	proxy      *httputil.ReverseProxy
	validators []plugins.ValidatingPlugin
	mutators   []plugins.MutatingPlugin
}

func NewHandler(ctx context.Context, target string, validators []plugins.ValidatingPlugin, mutators []plugins.MutatingPlugin) (*Handler, error) {
	log := logr.FromContextOrDiscard(ctx)
	h := new(Handler)
	h.validators = validators
	h.mutators = mutators
	log.Info("loaded plugins", "Mutators", len(mutators), "Validators", len(validators))
	h.client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
				CipherSuites: []uint16{
					tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
					tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				},
			},
			ForceAttemptHTTP2: true,
		},
	}

	// apply request id tracing
	tracer.Apply(h.client)

	// set up the reverse proxy
	if target == "" {
		log.Info("target URL must be a valid HTTP URI")
		return nil, errors.New("target url is blank")
	}
	t, err := url.Parse(target)
	if err != nil {
		log.Error(err, "failed to parse target URL")
		return nil, err
	}
	h.target = t
	h.proxy = httputil.NewSingleHostReverseProxy(t)
	h.proxy.Transport = h.client.Transport // use the transport from our client as it has enhanced tracing
	h.proxy.ModifyResponse = func(r *http.Response) error {
		log := logr.FromContextOrDiscard(r.Request.Context())
		log.V(1).Info("PROXY RESPONSE",
			"Status", r.StatusCode,
			"BodyBytesSent", r.ContentLength,
			"Proto", r.Proto,
		)
		metricProxyBytesReceive.Add(r.Request.Context(), r.ContentLength)
		metricProxyResponse.Add(r.Request.Context(), 1,
			attribute.Int("code", r.StatusCode),
			attribute.String("method", r.Request.Method),
			attribute.String("path", r.Request.URL.Path),
		)
		return nil
	}

	return h, nil
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log := logr.FromContextOrDiscard(r.Context())
	// log the request
	log.V(1).Info("%s - %s", r.Method, r.URL.Redacted())
	originalURL := *r.URL

	// invoke mutations
	for _, m := range h.mutators {
		if err := m.Mutate(r); err != nil {
			log.Error(err, "mutating plugin rejected request")
			http.Error(w, "something went wrong", http.StatusInternalServerError)
			return
		}
	}
	// invoke validations
	for _, v := range h.validators {
		if ok, err := v.IsValid(r); !ok || err != nil {
			log.Error(err, "rejecting request due to failed validation")
			http.Error(w, "bad request", http.StatusBadRequest)
			return
		}
	}

	// update request for proxying
	r.URL.Host = h.target.Host
	r.URL.Scheme = h.target.Scheme
	r.Header.Set(xForwardedHost, originalURL.Host)
	r.Header.Set(xForwardedProto, originalURL.Scheme)
	r.Host = h.target.Host

	log.V(1).Info("PROXY REQUEST",
		"Url", r.URL.Redacted(),
		"RemoteAddr", r.RemoteAddr,
		"Host", originalURL.Host,
		"Scheme", r.URL.Scheme,
		xForwardedProto, originalURL.Scheme,
		xForwardedHost, r.Header.Get("Host"),
		"UserAgent", r.UserAgent(),
		"BodyBytesSent", r.ContentLength,
		"Proto", r.Proto,
	)

	metricProxyBytesTransmit.Add(r.Context(), r.ContentLength)

	// serve in a non-blocking fashion
	h.proxy.ServeHTTP(w, r)
}
