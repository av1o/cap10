/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package proxy_test

import (
	"context"
	"github.com/go-logr/logr"
	"github.com/go-logr/logr/testr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/plugins"
	"gitlab.com/av1o/cap10/pkg/proxy"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewHandler(t *testing.T) {
	var cases = []struct {
		target string
		pass   bool
	}{
		{"http://localhost:8080", true},
		{"", false},
		{"%gh&%ij", false},
	}
	ctx := logr.NewContext(context.TODO(), testr.New(t))
	for _, tt := range cases {
		t.Run(tt.target, func(t *testing.T) {
			h, err := proxy.NewHandler(ctx, tt.target, nil, nil)
			if tt.pass {
				assert.NoError(t, err)
				assert.NotNil(t, h)
				return
			}
			assert.Error(t, err)
			assert.Nil(t, h)
		})
	}
}

type httpsValidator struct{}

func (v *httpsValidator) IsValid(r *http.Request) (bool, error) {
	return r.URL.Scheme == "https", nil
}

type encryptingMutator struct{}

func (m *encryptingMutator) Mutate(r *http.Request) error {
	if r.Host == "example.com" {
		r.URL.Scheme = "https"
	}
	return nil
}

func TestHandler_ServeHTTP(t *testing.T) {
	var cases = []struct {
		name string
		url  string
		code int
	}{
		{
			"http requests are blocked",
			"http://example.org",
			http.StatusBadRequest,
		},
		{
			"https requests are allowed",
			"https://example.org",
			http.StatusNotFound,
		},
		{
			"mutations work",
			"http://example.com",
			http.StatusNotFound,
		},
	}

	ts := httptest.NewServer(http.NotFoundHandler())
	defer ts.Close()

	validators := []plugins.ValidatingPlugin{&httpsValidator{}}
	mutators := []plugins.MutatingPlugin{&encryptingMutator{}}

	ctx := logr.NewContext(context.TODO(), testr.New(t))
	h, err := proxy.NewHandler(ctx, ts.URL, validators, mutators)
	assert.NoError(t, err)

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.HTTPStatusCode(t, h.ServeHTTP, http.MethodGet, tt.url, nil, tt.code)
		})
	}
}
