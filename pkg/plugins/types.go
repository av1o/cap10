/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package plugins

import "net/http"

const (
	SymbolValidating = "ValidatingPlugin"
	SymbolMutating   = "MutatingPlugin"
)

// ValidatingPlugin takes a request and determines
// whether the request should be allowed to continue.
//
// ValidatingPlugin's will always be called AFTER the
// MutatingPlugin's
//
// Returning false or an error will cause a 400 Bad Request
// to be returned to the requester.
type ValidatingPlugin interface {
	IsValid(r *http.Request) (bool, error)
}

// MutatingPlugin takes a request and makes arbitrary
// modifications before passing it to the reverse proxy.
//
// Returning an error will cause a 500 Internal Server Error
// to be returned to the requester.
type MutatingPlugin interface {
	Mutate(r *http.Request) error
}
