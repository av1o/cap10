/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package plugins

import (
	"context"
	"github.com/go-logr/logr"
	"io/fs"
	"path/filepath"
	"plugin"
	"strings"
)

// Loader is used to load Cap10
// plugins
type Loader struct {
	dirs    []string
	plugins []*plugin.Plugin
	log     logr.Logger
}

// NewLoader creates a new instance
// of Loader
func NewLoader(ctx context.Context, paths string) *Loader {
	l := new(Loader)
	l.dirs = strings.Split(paths, ",")
	l.log = logr.FromContextOrDiscard(ctx)

	return l
}

func (l *Loader) LookupV() []ValidatingPlugin {
	log := l.log
	var plugins []ValidatingPlugin
	for _, p := range l.plugins {
		sym, err := p.Lookup(SymbolValidating)
		if err != nil {
			log.Error(err, "failed to locate symbol in plugin (probably wrong plugin type)", "Symbol", SymbolValidating)
		}
		v, ok := sym.(ValidatingPlugin)
		if !ok {
			log.Info("unexpected type from module symbol")
			continue
		}
		plugins = append(plugins, v)
	}
	log.Info("successfully loaded validating plugins", "Count", len(plugins))
	return plugins
}

func (l *Loader) LookupM() []MutatingPlugin {
	log := l.log
	var plugins []MutatingPlugin
	for _, p := range l.plugins {
		sym, err := p.Lookup(SymbolMutating)
		if err != nil {
			log.Error(err, "failed to locate symbol in plugin (probably wrong plugin type)", "Symbol", SymbolMutating)
		}
		v, ok := sym.(MutatingPlugin)
		if !ok {
			log.Info("unexpected type from module symbol")
			continue
		}
		plugins = append(plugins, v)
	}
	log.Info("successfully loaded mutating plugins", "Count", len(plugins))
	return plugins
}

func (l *Loader) LoadAll() error {
	l.log.Info("attempting to load all .so plugins found", "Dirs", l.dirs)
	for _, d := range l.dirs {
		plugins, err := l.loadInDir(d)
		if err != nil {
			return err
		}
		l.plugins = append(l.plugins, plugins...)
	}
	return nil
}

// loadInDir loads all plugins found in a given
// directory
func (l *Loader) loadInDir(dir string) ([]*plugin.Plugin, error) {
	log := l.log.WithValues("Directory", dir)
	log.V(1).Info("loading plugins in directory")
	var plugins []*plugin.Plugin
	files, err := l.walkMatch(dir)
	if err != nil {
		log.Error(err, "failed to load plugins in directory")
		return nil, err
	}
	for _, f := range files {
		plug, err := plugin.Open(f)
		if err != nil {
			log.Error(err, "failed to load plugin from file", "File", f)
			return nil, err
		}
		plugins = append(plugins, plug)
		log.V(1).Info("successfully loaded plugin", "File", f)
	}
	log.V(1).Info("successfully loaded plugins")
	return plugins, nil
}

// walkMatch recursively walks a directory and returns matches
//
// https://stackoverflow.com/a/55300382
func (*Loader) walkMatch(root string) ([]string, error) {
	var matches []string
	err := filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil
		}
		if m, err := filepath.Match("*.so", filepath.Base(path)); err != nil {
			return err
		} else if m {
			matches = append(matches, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return matches, nil
}
